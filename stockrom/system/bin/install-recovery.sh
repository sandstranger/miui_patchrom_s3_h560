#!/system/bin/sh
if [ -f /system/etc/recovery-transform.sh ]; then
  exec sh /system/etc/recovery-transform.sh 11878400 8baead7ada77e8309c69a5400b8fdee90ed6d85d 8599552 9c4c104645d80710f45b7b5b07bf50af7a148e0a
fi

if ! applypatch -c EMMC:/dev/block/platform/mtk-msdc.0/by-name/recovery:11878400:8baead7ada77e8309c69a5400b8fdee90ed6d85d; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/mtk-msdc.0/by-name/boot:8599552:9c4c104645d80710f45b7b5b07bf50af7a148e0a EMMC:/dev/block/platform/mtk-msdc.0/by-name/recovery 8baead7ada77e8309c69a5400b8fdee90ed6d85d 11878400 9c4c104645d80710f45b7b5b07bf50af7a148e0a:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
